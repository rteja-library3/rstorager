package rstorager_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rstorager"
)

func TestMergeLocalStorageOptions(t *testing.T) {
	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(rstorager.StorageFileSystemOS{})

	opt := rstorager.MergeLocalStorageOptions(opts)

	assert.NotNil(t, opt, "[TestMergeLocalStorageOptions] Should not nil")
}

func TestMergeLocalStorageOptionsWithDefault(t *testing.T) {
	opts := rstorager.NewLocalStorageOptions().
		SetPath("test")

	opt := rstorager.MergeLocalStorageOptions(opts)

	assert.NotNil(t, opt, "[TestMergeLocalStorageOptionsWithDefault] Should not nil")
}
