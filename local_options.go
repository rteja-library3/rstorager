package rstorager

type LocalStorageOptions struct {
	Path       string
	FileSystem StoragerFileSystem
}

func (l *LocalStorageOptions) SetPath(path string) *LocalStorageOptions {
	l.Path = path
	return l
}

func (l *LocalStorageOptions) SetFileSystem(fileSystem StoragerFileSystem) *LocalStorageOptions {
	l.FileSystem = fileSystem
	return l
}

func NewLocalStorageOptions() *LocalStorageOptions {
	return &LocalStorageOptions{}
}

func MergeLocalStorageOptions(opts ...*LocalStorageOptions) *LocalStorageOptions {
	opt := NewLocalStorageOptions()

	for _, o := range opts {
		if o.Path != "" {
			opt.Path = o.Path
		}

		if o.FileSystem != nil {
			opt.FileSystem = o.FileSystem
		}
	}

	if opt.FileSystem == nil {
		opt.FileSystem = StorageFileSystemOS{}
	}

	return opt
}
