package rstorager

import (
	"bytes"
	"context"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/rteja-library3/rhelper"
)

type LocalStorage struct {
	path string
	fs   StoragerFileSystem
}

func NewLocalStorager(opts ...*LocalStorageOptions) Storage {
	opt := MergeLocalStorageOptions(opts...)

	return &LocalStorage{
		path: opt.Path,
		fs:   opt.FileSystem,
	}
}

func (l LocalStorage) GetObject(ctx context.Context, filename string, opts ...*GetOptions) (reader io.Reader, err error) {
	var file StoragerFile

	file, err = l.fs.Open(filepath.Join(l.path, filename))
	if err != nil {
		// if possible we should replace to our own error
		return
	}
	defer file.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(file)

	reader = buf
	return
}

func (l LocalStorage) GetObjectAsURL(ctx context.Context, filename string, opts ...*GetOptions) (url string, err error) {
	return
}

func (l LocalStorage) PutObject(ctx context.Context, filename string, reader io.Reader, objectSize int64, opts ...*PutOptions) (id string, err error) {
	var file StoragerFile
	var path string = filepath.Join(l.path, filename)
	id = rhelper.GetAlphanumeric(filename) + "-" + rhelper.GenerateRandomString(rhelper.GetAlphanumeric(path), 6)

	file, err = l.fs.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return
	}
	defer file.Close()

	_, err = io.Copy(file, reader)
	return
}

func (l LocalStorage) DeleteObject(ctx context.Context, filename string, opts ...*RemoveOptions) (err error) {
	err = l.fs.Remove(filepath.Join(l.path, filename))
	return
}

func (l LocalStorage) DeleteObjects(ctx context.Context, filenames []string, opts ...*RemoveOptions) (err error) {
	for _, file := range filenames {
		err = l.fs.Remove(filepath.Join(l.path, file))
		if err != nil {
			return
		}
	}

	return
}

func (l LocalStorage) DeleteFolders(ctx context.Context, folders []string, opts ...*RemoveOptions) (err error) {
	return
}
