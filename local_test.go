package rstorager_test

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rstorager"
	"gitlab.com/rteja-library3/rstorager/mocks"
)

func TestGetObjectSuccess(t *testing.T) {
	fl := new(mocks.StoragerFile)

	fl.On("Read", mock.Anything).
		Return(0, io.EOF)

	fl.On("Close").
		Return(nil)

	fs := new(mocks.StoragerFileSystem)

	fs.On("Open", mock.Anything).
		Return(fl, nil)

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	reader, err := local.GetObject(context.Background(), "test.txt")

	assert.NoError(t, err, "[TestGetObjectSuccess] Should not error")
	assert.NotNil(t, reader, "[TestGetObjectSuccess] Reader should not nil")
}

func TestGetObjectErr(t *testing.T) {
	// fl := new(mocks.StoragerFile)

	fs := new(mocks.StoragerFileSystem)

	fs.On("Open", mock.Anything).
		Return(nil, fmt.Errorf("test"))

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	reader, err := local.GetObject(context.Background(), "test.txt")

	assert.Error(t, err, "[TestGetObjectSuccess] Should error")
	assert.Nil(t, reader, "[TestGetObjectSuccess] Reader should nil")
}

func TestGetObjectAsURLSuccess(t *testing.T) {
	fs := new(mocks.StoragerFileSystem)

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	_, err := local.GetObjectAsURL(context.Background(), "test.txt")

	assert.NoError(t, err, "[TestGetObjectAsURLSuccess] Should not error")
}

func TestDeleteObjectSuccess(t *testing.T) {
	fs := new(mocks.StoragerFileSystem)

	fs.On("Remove", mock.Anything).
		Return(nil)

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	err := local.DeleteObject(context.Background(), "test.txt")

	assert.NoError(t, err, "[TestDeleteObjectSuccess] Should not error")
}

func TestDeleteObjectError(t *testing.T) {
	fs := new(mocks.StoragerFileSystem)

	fs.On("Remove", mock.Anything).
		Return(fmt.Errorf("error"))

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	err := local.DeleteObject(context.Background(), "test.txt")

	assert.Error(t, err, "[TestDeleteObjectError] Should error")
}

func TestDeleteObjectsSuccess(t *testing.T) {
	fs := new(mocks.StoragerFileSystem)

	fs.On("Remove", mock.Anything).
		Return(nil)

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	err := local.DeleteObjects(context.Background(), []string{"test.txt"})

	assert.NoError(t, err, "[TestDeleteObjectsSuccess] Should not error")
}

func TestDeleteObjectsError(t *testing.T) {
	fs := new(mocks.StoragerFileSystem)

	fs.On("Remove", mock.Anything).
		Return(fmt.Errorf("test"))

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	err := local.DeleteObjects(context.Background(), []string{"test.txt"})

	assert.Error(t, err, "[TestDeleteObjectsError] Should error")
}

func TestDeleteFoldersSuccess(t *testing.T) {
	fs := new(mocks.StoragerFileSystem)

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	err := local.DeleteFolders(context.Background(), []string{"test.txt"})

	assert.NoError(t, err, "[TestDeleteFoldersSuccess] Should not error")
}

func TestPutObjectSuccess(t *testing.T) {
	fl := new(mocks.StoragerFile)

	fl.On("Write", mock.Anything).
		Return(0, io.EOF)

	fl.On("Close").
		Return(nil)

	fs := new(mocks.StoragerFileSystem)

	fs.
		On("OpenFile", mock.Anything, mock.Anything, mock.Anything).
		Return(fl, nil)

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	reader := new(bytes.Buffer)

	id, err := local.PutObject(context.Background(), "", reader, 10)

	assert.NoError(t, err, "[TestPutObjectSuccess] Should not error")
	assert.NotEmpty(t, id, "[TestPutObjectSuccess] Should not empty")
}

func TestPutObjectError(t *testing.T) {
	fl := new(mocks.StoragerFile)

	fs := new(mocks.StoragerFileSystem)

	fs.
		On("OpenFile", mock.Anything, mock.Anything, mock.Anything).
		Return(fl, fmt.Errorf("not found"))

	opts := rstorager.NewLocalStorageOptions().
		SetPath("test").
		SetFileSystem(fs)

	local := rstorager.NewLocalStorager(opts)

	reader := new(bytes.Buffer)

	id, err := local.PutObject(context.Background(), "", reader, 10)

	assert.Error(t, err, "[TestPutObjectSuccess] Should error")
	assert.NotEmpty(t, id, "[TestPutObjectSuccess] Should not empty")
}
