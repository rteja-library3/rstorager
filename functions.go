package rstorager

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type CreateStorageS3ObjectClient func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client StorageS3ObjectClient, err error)
type CreateStorageS3PresignClientObjectClient func(ctx context.Context, conf *aws.Config, optFns ...func(*s3.PresignOptions)) StorageS3PresignObjectClient

var (
	DefaultCreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client StorageS3ObjectClient, err error) {
		var cfg aws.Config

		optFns = append(optFns, config.WithCredentialsProvider(conf.Credentials))
		optFns = append(optFns, config.WithRegion(conf.Region))

		cfg, err = config.LoadDefaultConfig(
			ctx,
			optFns...,
		)
		if err != nil {
			return
		}

		client = s3.NewFromConfig(cfg)
		return
	}

	DefaultCreateStorageS3PresignClientObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*s3.PresignOptions)) StorageS3PresignObjectClient {
		var cfg aws.Config

		cfg, _ = config.LoadDefaultConfig(
			ctx,
			config.WithCredentialsProvider(conf.Credentials),
			config.WithRegion(conf.Region),
		)

		return s3.NewPresignClient(s3.NewFromConfig(cfg), optFns...)
	}
)
