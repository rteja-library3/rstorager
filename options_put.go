package rstorager

import "time"

type PutOptions struct {
	ContentType             string
	UserMetadata            map[string]string
	UserTags                map[string]string
	ContentEncoding         string
	ContentDisposition      string
	ContentLanguage         string
	CacheControl            string
	WebsiteRedirectLocation string
	Expires                 time.Time
	VersionID               string
}

func (g *PutOptions) SetContentType(ContentType string) *PutOptions {
	g.ContentType = ContentType
	return g
}

func (g *PutOptions) SetUserMetadata(UserMetadata map[string]string) *PutOptions {
	g.UserMetadata = UserMetadata
	return g
}

func (g *PutOptions) SetUserTags(UserTags map[string]string) *PutOptions {
	g.UserTags = UserTags
	return g
}

func (g *PutOptions) SetContentEncoding(ContentEncoding string) *PutOptions {
	g.ContentEncoding = ContentEncoding
	return g
}

func (g *PutOptions) SetContentDisposition(ContentDisposition string) *PutOptions {
	g.ContentDisposition = ContentDisposition
	return g
}

func (g *PutOptions) SetContentLanguage(ContentLanguage string) *PutOptions {
	g.ContentLanguage = ContentLanguage
	return g
}

func (g *PutOptions) SetCacheControl(CacheControl string) *PutOptions {
	g.CacheControl = CacheControl
	return g
}

func (g *PutOptions) SetWebsiteRedirectLocation(WebsiteRedirectLocation string) *PutOptions {
	g.WebsiteRedirectLocation = WebsiteRedirectLocation
	return g
}

func (g *PutOptions) SetExpires(Expires time.Time) *PutOptions {
	g.Expires = Expires
	return g
}

func (g *PutOptions) SetVersionID(VersionID string) *PutOptions {
	g.VersionID = VersionID
	return g
}

func NewPutOptions() *PutOptions {
	return &PutOptions{}
}

func MergePutOptions(options ...*PutOptions) *PutOptions {
	opt := NewPutOptions()

	for _, option := range options {
		if option.ContentType != "" {
			opt.ContentType = option.ContentType
		}

		if option.UserMetadata != nil {
			opt.UserMetadata = option.UserMetadata
		}

		if option.UserTags != nil {
			opt.UserTags = option.UserTags
		}

		if option.ContentEncoding != "" {
			opt.ContentEncoding = option.ContentEncoding
		}

		if option.ContentDisposition != "" {
			opt.ContentDisposition = option.ContentDisposition
		}

		if option.ContentLanguage != "" {
			opt.ContentLanguage = option.ContentLanguage
		}

		if option.CacheControl != "" {
			opt.CacheControl = option.CacheControl
		}

		if option.WebsiteRedirectLocation != "" {
			opt.WebsiteRedirectLocation = option.WebsiteRedirectLocation
		}

		if !option.Expires.IsZero() {
			opt.Expires = option.Expires
		}

		if option.VersionID != "" {
			opt.VersionID = option.VersionID
		}
	}

	return opt
}
