package rstorager

type RemoveOptions struct {
	VersionID string
}

func (g *RemoveOptions) SetVersionID(VersionID string) *RemoveOptions {
	g.VersionID = VersionID
	return g
}

func NewRemoveOptions() *RemoveOptions {
	return &RemoveOptions{}
}

func MergeRemoveOptions(options ...*RemoveOptions) *RemoveOptions {
	opt := NewRemoveOptions()

	for _, option := range options {
		if option.VersionID != "" {
			opt.VersionID = option.VersionID
		}
	}

	return opt
}
