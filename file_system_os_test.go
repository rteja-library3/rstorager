package rstorager_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rstorager"
)

func TestOpen(t *testing.T) {
	fs := rstorager.StorageFileSystemOS{}

	r, err := fs.Open("test.txt")

	assert.Error(t, err, "[TestOpen] should error")
	assert.Nil(t, r, "[TestOpen] Reader should nil")
}

func TestOpenFile(t *testing.T) {
	fs := rstorager.StorageFileSystemOS{}

	r, err := fs.OpenFile("test.txt", 0, os.ModeAppend)

	assert.Error(t, err, "[TestOpenFile] should error")
	assert.Nil(t, r, "[TestOpenFile] Reader should nil")
}

func TestRemove(t *testing.T) {
	fs := rstorager.StorageFileSystemOS{}

	err := fs.Remove("test.txt")

	assert.Error(t, err, "[TestRemove] should error")
}
