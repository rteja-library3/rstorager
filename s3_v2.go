package rstorager

import (
	"bytes"
	"context"
	"io"

	"github.com/aws/aws-sdk-go-v2/aws"
	v4 "github.com/aws/aws-sdk-go-v2/aws/signer/v4"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
)

type S3V2Storage struct {
	conf           *aws.Config
	clientFunction CreateStorageS3ObjectClient
	presignClienFn CreateStorageS3PresignClientObjectClient
	bucketName     string
}

func NewS3V2Storage(conf *aws.Config, clientFunction CreateStorageS3ObjectClient, presignClienFn CreateStorageS3PresignClientObjectClient, bucketName string) Storage {
	if clientFunction == nil {
		clientFunction = DefaultCreateStorageS3ObjectClient
	}

	if presignClienFn == nil {
		presignClienFn = DefaultCreateStorageS3PresignClientObjectClient
	}

	return &S3V2Storage{
		conf:           conf,
		clientFunction: clientFunction,
		presignClienFn: presignClienFn,
		bucketName:     bucketName,
	}
}

func (s S3V2Storage) GetObject(ctx context.Context, filename string, opts ...*GetOptions) (reader io.Reader, err error) {
	var client StorageS3ObjectClient

	client, err = s.clientFunction(ctx, s.conf)
	if err != nil {
		return
	}

	input := &s3.GetObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(filename),
	}

	var output *s3.GetObjectOutput

	output, err = client.GetObject(ctx, input)
	if err != nil {
		return
	}
	defer output.Body.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(output.Body)

	reader = buf
	return
}

func (s S3V2Storage) GetObjectAsURL(ctx context.Context, filename string, opts ...*GetOptions) (url string, err error) {
	input := &s3.GetObjectInput{
		Bucket: aws.String(s.bucketName),
		Key:    aws.String(filename),
	}

	var preSignURL *v4.PresignedHTTPRequest

	preSignURL, err = s.
		presignClienFn(ctx, s.conf).
		PresignGetObject(ctx, input)
	if err != nil {
		return
	}

	url = preSignURL.URL
	return
}

func (s S3V2Storage) PutObject(ctx context.Context, filename string, reader io.Reader, objectSize int64, options ...*PutOptions) (id string, err error) {
	opts := MergePutOptions(options...)

	var client StorageS3ObjectClient

	client, err = s.clientFunction(ctx, s.conf)
	if err != nil {
		return
	}

	input := &s3.PutObjectInput{
		Body:                    reader,
		Bucket:                  aws.String(s.bucketName),
		Key:                     aws.String(filename),
		ContentLength:           objectSize,
		ContentType:             aws.String(opts.ContentType),
		ContentDisposition:      aws.String(opts.ContentDisposition),
		CacheControl:            aws.String(opts.CacheControl),
		ContentEncoding:         aws.String(opts.ContentEncoding),
		ContentLanguage:         aws.String(opts.ContentLanguage),
		Metadata:                opts.UserMetadata,
		WebsiteRedirectLocation: aws.String(opts.WebsiteRedirectLocation),
		Expires:                 aws.Time(opts.Expires),
	}

	id = filename

	_, err = client.PutObject(ctx, input)
	return
}

func (s S3V2Storage) DeleteObject(ctx context.Context, filename string, opts ...*RemoveOptions) (err error) {
	var client StorageS3ObjectClient

	client, err = s.clientFunction(ctx, s.conf)
	if err != nil {
		return
	}

	objects := []types.ObjectIdentifier{
		{
			Key: aws.String(filename),
		},
	}

	_, err = client.DeleteObjects(ctx, &s3.DeleteObjectsInput{
		Bucket: aws.String(s.bucketName),
		Delete: &types.Delete{
			Objects: objects,
		},
	})
	if err != nil {
		return
	}

	return
}

func (s S3V2Storage) DeleteObjects(ctx context.Context, filenames []string, opts ...*RemoveOptions) (err error) {
	var client StorageS3ObjectClient

	client, err = s.clientFunction(ctx, s.conf)
	if err != nil {
		return
	}

	var objects []types.ObjectIdentifier = make([]types.ObjectIdentifier, len(filenames))

	for rr, filename := range filenames {
		objects[rr] = types.ObjectIdentifier{
			Key: aws.String(filename),
		}
	}

	_, err = client.DeleteObjects(ctx, &s3.DeleteObjectsInput{
		Bucket: aws.String(s.bucketName),
		Delete: &types.Delete{
			Objects: objects,
		},
	})
	if err != nil {
		return
	}

	return
}

func (s S3V2Storage) DeleteFolders(ctx context.Context, folders []string, opts ...*RemoveOptions) (err error) {
	var objects []types.ObjectIdentifier = make([]types.ObjectIdentifier, 0)
	var listObjects *s3.ListObjectsV2Output
	var client StorageS3ObjectClient

	client, err = s.clientFunction(ctx, s.conf)
	if err != nil {
		return
	}

	idx := 0
	var startAfter *string

	for idx < len(folders) {
		folder := folders[idx]

		listObjects, err = client.ListObjectsV2(ctx, &s3.ListObjectsV2Input{
			Bucket:     &s.bucketName,
			Prefix:     aws.String(folder),
			StartAfter: startAfter,
		})
		if err != nil {
			return
		}

		for _, content := range listObjects.Contents {
			objects = append(objects, types.ObjectIdentifier{
				Key: content.Key,
			})
		}

		if listObjects.StartAfter == nil {
			idx++
		}

		startAfter = listObjects.StartAfter
	}

	_, err = client.DeleteObjects(ctx, &s3.DeleteObjectsInput{
		Bucket: aws.String(s.bucketName),
		Delete: &types.Delete{
			Objects: objects,
		},
	})
	if err != nil {
		return
	}

	return
}
