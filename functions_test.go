package rstorager_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rstorager"
)

func TestDefaultCreateStorageS3ObjectClientSuccess(t *testing.T) {
	var cfg *aws.Config = &aws.Config{
		Region: "",
	}

	cl, err := rstorager.
		DefaultCreateStorageS3ObjectClient(context.Background(), cfg)

	assert.NoError(t, err, "[TestDefaultCreateStorageS3ObjectClientSuccess] Should not error")
	assert.NotNil(t, cl, "[TestDefaultCreateStorageS3ObjectClientSuccess] Should not nil")
}

func TestDefaultCreateStorageS3ObjectClientError(t *testing.T) {
	var cfg *aws.Config = &aws.Config{
		Region: "",
	}

	_, err := rstorager.
		DefaultCreateStorageS3ObjectClient(context.Background(), cfg, func(lo *config.LoadOptions) error {
			return fmt.Errorf("xx")
		})

	assert.Error(t, err, "[TestDefaultCreateStorageS3ObjectClientError] Should error")
}

func TestDefaultCreateStorageS3PresignClientObjectClientSuccess(t *testing.T) {
	var cfg *aws.Config = &aws.Config{
		Region: "",
	}

	err := rstorager.
		DefaultCreateStorageS3PresignClientObjectClient(context.Background(), cfg)

	assert.NotNil(t, err, "[TestDefaultCreateStorageS3PresignClientObjectClientSuccess] Should not error")
}
