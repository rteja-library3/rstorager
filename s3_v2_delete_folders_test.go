package rstorager_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rstorager"
	"gitlab.com/rteja-library3/rstorager/mocks"
)

func TestS3DeleteFoldersSuccess(t *testing.T) {
	listObjs := new(s3.ListObjectsV2Output)
	listObjs.Contents = []types.Object{
		{
			Key: aws.String("aa"),
		},
	}

	s3Output := new(s3.DeleteObjectsOutput)

	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("DeleteObjects", mock.Anything, mock.Anything).
		Return(s3Output, nil)

	s3Client.On("ListObjectsV2", mock.Anything, mock.Anything).
		Return(listObjs, nil)

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	err := s3.DeleteFolders(context.Background(), []string{""})

	assert.NoError(t, err, "[TestS3DeleteFoldersSuccess] Error Should nil")
	s3Client.AssertExpectations(t)
}

func TestS3DeleteFoldersClientError(t *testing.T) {
	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return nil, fmt.Errorf("test")
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	err := s3.DeleteFolders(context.Background(), []string{""})

	assert.Error(t, err, "[TestS3DeleteFoldersClientError] Should error")
}

func TestS3DeleteFoldersListObjectError(t *testing.T) {
	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("ListObjectsV2", mock.Anything, mock.Anything).
		Return(nil, fmt.Errorf("test"))

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	err := s3.DeleteFolders(context.Background(), []string{""})

	assert.Error(t, err, "[TestS3DeleteFoldersListObjectError] Should error")

	s3Client.AssertExpectations(t)
}

func TestS3DeleteFoldersDeleteObjectsError(t *testing.T) {
	listObjs := new(s3.ListObjectsV2Output)
	listObjs.Contents = []types.Object{
		{
			Key: aws.String("aa"),
		},
	}

	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("DeleteObjects", mock.Anything, mock.Anything).
		Return(nil, fmt.Errorf("test"))

	s3Client.On("ListObjectsV2", mock.Anything, mock.Anything).
		Return(listObjs, nil)

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	err := s3.DeleteFolders(context.Background(), []string{""})

	assert.Error(t, err, "[TestS3DeleteFoldersDeleteObjectsError] Should error")

	s3Client.AssertExpectations(t)
}
