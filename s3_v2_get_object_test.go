package rstorager_test

import (
	"context"
	"fmt"
	"io"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rstorager"
	"gitlab.com/rteja-library3/rstorager/mocks"
)

func TestS3GetObjectSuccess(t *testing.T) {
	rc := new(mocks.StoragerFileReader)

	rc.On("Read", mock.Anything).
		Return(0, io.EOF)

	rc.On("Close").
		Return(nil)

	s3GetObjOut := new(s3.GetObjectOutput)
	s3GetObjOut.Body = rc

	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("GetObject", mock.Anything, mock.Anything, mock.Anything).
		Return(s3GetObjOut, nil)

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	r, err := s3.GetObject(context.Background(), "")

	assert.NoError(t, err, "[TestS3GetObjectSuccess] Error Should nil")
	assert.NotNil(t, r, "[TestS3GetObjectSuccess] Reader should not nil")

	rc.AssertExpectations(t)
	s3Client.AssertExpectations(t)
}

func TestS3GetObjectClientError(t *testing.T) {
	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return nil, fmt.Errorf("test")
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	r, err := s3.GetObject(context.Background(), "")

	assert.Error(t, err, "[TestS3GetObjectSuccess] Should error ")
	assert.Nil(t, r, "[TestS3GetObjectSuccess] Reader should nil")
}

func TestS3GetObjectError(t *testing.T) {
	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("GetObject", mock.Anything, mock.Anything, mock.Anything).
		Return(nil, fmt.Errorf("error"))

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	r, err := s3.GetObject(context.Background(), "")

	assert.Error(t, err, "[TestS3GetObjectSuccess] Should error ")
	assert.Nil(t, r, "[TestS3GetObjectSuccess] Reader should nil")

	s3Client.AssertExpectations(t)
}
