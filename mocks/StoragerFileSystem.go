// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import fs "io/fs"
import mock "github.com/stretchr/testify/mock"
import rstorager "gitlab.com/rteja-library3/rstorager"

// StoragerFileSystem is an autogenerated mock type for the StoragerFileSystem type
type StoragerFileSystem struct {
	mock.Mock
}

// Open provides a mock function with given fields: name
func (_m *StoragerFileSystem) Open(name string) (rstorager.StoragerFile, error) {
	ret := _m.Called(name)

	var r0 rstorager.StoragerFile
	if rf, ok := ret.Get(0).(func(string) rstorager.StoragerFile); ok {
		r0 = rf(name)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(rstorager.StoragerFile)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(name)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// OpenFile provides a mock function with given fields: name, flag, perm
func (_m *StoragerFileSystem) OpenFile(name string, flag int, perm fs.FileMode) (rstorager.StoragerFile, error) {
	ret := _m.Called(name, flag, perm)

	var r0 rstorager.StoragerFile
	if rf, ok := ret.Get(0).(func(string, int, fs.FileMode) rstorager.StoragerFile); ok {
		r0 = rf(name, flag, perm)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(rstorager.StoragerFile)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, int, fs.FileMode) error); ok {
		r1 = rf(name, flag, perm)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Remove provides a mock function with given fields: name
func (_m *StoragerFileSystem) Remove(name string) error {
	ret := _m.Called(name)

	var r0 error
	if rf, ok := ret.Get(0).(func(string) error); ok {
		r0 = rf(name)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
