package rstorager_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rstorager"
)

func TestNewS3(t *testing.T) {
	s3 := rstorager.NewS3V2Storage(nil, nil, nil, "")

	assert.NotNil(t, s3, "[TestNewS3] Should not nil")
}
