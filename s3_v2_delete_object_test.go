package rstorager_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rstorager"
	"gitlab.com/rteja-library3/rstorager/mocks"
)

func TestS3DeleteObjectSuccess(t *testing.T) {
	s3Output := new(s3.DeleteObjectsOutput)

	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("DeleteObjects", mock.Anything, mock.Anything).
		Return(s3Output, nil)

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	err := s3.DeleteObject(context.Background(), "")

	assert.NoError(t, err, "[TestS3DeleteObjectSuccess] Error Should nil")
}

func TestS3DeleteObjectClientError(t *testing.T) {
	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return nil, fmt.Errorf("test")
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	err := s3.DeleteObject(context.Background(), "")

	assert.Error(t, err, "[TestS3DeleteObjectClientError] Should error")
}

func TestS3DeleteObjectError(t *testing.T) {
	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("DeleteObjects", mock.Anything, mock.Anything).
		Return(nil, fmt.Errorf("error"))

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	err := s3.DeleteObject(context.Background(), "")

	assert.Error(t, err, "[TestS3DeleteObjectError] Should error")
}
