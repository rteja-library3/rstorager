package rstorager

import (
	"context"
	"io"
)

type Readable interface {
	GetObject(ctx context.Context, filename string, opts ...*GetOptions) (reader io.Reader, err error)
	GetObjectAsURL(ctx context.Context, filename string, opts ...*GetOptions) (url string, err error)
}

type Writeable interface {
	PutObject(ctx context.Context, filename string, reader io.Reader, objectSize int64, opts ...*PutOptions) (id string, err error)
	DeleteObject(ctx context.Context, filename string, opts ...*RemoveOptions) (err error)
	DeleteObjects(ctx context.Context, filenames []string, opts ...*RemoveOptions) (err error)
	DeleteFolders(ctx context.Context, folders []string, opts ...*RemoveOptions) (err error)
}

type Storage interface {
	Readable
	Writeable
}
