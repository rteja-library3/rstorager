package rstorager

type GetOptions struct {
	VersionID string
}

func (g *GetOptions) SetVersionID(VersionID string) *GetOptions {
	g.VersionID = VersionID
	return g
}

func NewGetOptions() *GetOptions {
	return &GetOptions{}
}

func MergeGetOptions(options ...*GetOptions) *GetOptions {
	opt := NewGetOptions()

	for _, option := range options {
		if option.VersionID != "" {
			opt.VersionID = option.VersionID
		}
	}

	return opt
}
