package rstorager_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rstorager"
	"gitlab.com/rteja-library3/rstorager/mocks"
)

func TestS3PutObjectSuccess(t *testing.T) {
	output := new(s3.PutObjectOutput)

	s3Client := new(mocks.StorageS3ObjectClient)

	s3Client.On("PutObject", mock.Anything, mock.Anything, mock.Anything).
		Return(output, nil)

	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return s3Client, nil
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	rdr := new(mocks.StoragerFileReader)

	id, err := s3.PutObject(context.Background(), "test-01.txt", rdr, 10)

	assert.NoError(t, err, "[TestS3PutObjectSuccess] Error Should nil")
	assert.NotEmpty(t, id, "[TestS3PutObjectSuccess] Id should not empty")
	assert.Equal(t, "test-01.txt", id, "[TestS3PutObjectSuccess] Id should \"test-01.txt\"")

	s3Client.AssertExpectations(t)
}

func TestS3PutObjectClientError(t *testing.T) {
	var s3ObjClient rstorager.CreateStorageS3ObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*config.LoadOptions) error) (client rstorager.StorageS3ObjectClient, err error) {
		return nil, fmt.Errorf("test")
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		s3ObjClient,
		nil,
		"",
	)

	rdr := new(mocks.StoragerFileReader)

	id, err := s3.PutObject(context.Background(), "test-01.txt", rdr, 10)

	assert.Error(t, err, "[TestS3PutObjectClientError] Should error ")
	assert.Empty(t, id, "[TestS3PutObjectClientError] Reader should nil")
}
