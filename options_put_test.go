package rstorager_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rstorager"
)

func TestMergePutOptions(t *testing.T) {
	opt := rstorager.
		NewPutOptions().
		SetContentType("application/json").
		SetUserMetadata(map[string]string{}).
		SetUserTags(map[string]string{}).
		SetContentEncoding("application/json").
		SetContentDisposition("application/json").
		SetContentLanguage("application/json").
		SetCacheControl("application/json").
		SetWebsiteRedirectLocation("application/json").
		SetExpires(time.Now().Add(1 * time.Hour)).
		SetVersionID("v1.0.1")

	opts := rstorager.MergePutOptions(opt)

	assert.NotNil(t, opts, "[TestMergePutOptions] Should not nil")
}
