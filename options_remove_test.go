package rstorager_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rstorager"
)

func TestMergeRemoveOptions(t *testing.T) {
	opt := rstorager.
		NewRemoveOptions().
		SetVersionID("v1.0.1")

	opts := rstorager.MergeRemoveOptions(opt)

	assert.NotNil(t, opts, "[TestMergeRemoveOptions] Should not nil")
}
