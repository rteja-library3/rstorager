package rstorager_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rstorager"
)

func TestMergeGetOptions(t *testing.T) {
	opt := rstorager.
		NewGetOptions().
		SetVersionID("v1.0.1")

	opts := rstorager.MergeGetOptions(opt)

	assert.NotNil(t, opts, "[TestMergeGetOptions] Should not nil")
}
