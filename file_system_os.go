package rstorager

import "os"

type StorageFileSystemOS struct{}

func (f StorageFileSystemOS) Open(name string) (StoragerFile, error) {
	return os.Open(name)
}

func (f StorageFileSystemOS) OpenFile(name string, flag int, perm os.FileMode) (StoragerFile, error) {
	return os.OpenFile(name, flag, perm)
}

func (f StorageFileSystemOS) Remove(name string) error {
	return os.Remove(name)
}
