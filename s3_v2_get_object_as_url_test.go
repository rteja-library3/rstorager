package rstorager_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	v4 "github.com/aws/aws-sdk-go-v2/aws/signer/v4"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/rteja-library3/rstorager"
	"gitlab.com/rteja-library3/rstorager/mocks"
)

func TestS3GetAsURLObjectSuccess(t *testing.T) {
	s3GetObjOut := new(v4.PresignedHTTPRequest)
	s3GetObjOut.URL = "https://google.com/"

	s3Client := new(mocks.StorageS3PresignObjectClient)

	s3Client.On("PresignGetObject", mock.Anything, mock.Anything).
		Return(s3GetObjOut, nil)

	var s3ObjClient rstorager.CreateStorageS3PresignClientObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*s3.PresignOptions)) rstorager.StorageS3PresignObjectClient {
		return s3Client
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		nil,
		s3ObjClient,
		"tes",
	)

	url, err := s3.GetObjectAsURL(context.Background(), "")

	assert.NoError(t, err, "[TestS3GetAsURLObjectSuccess] Error Should nil")
	assert.NotEmpty(t, url, "[TestS3GetAsURLObjectSuccess] URL should not empty")
	assert.Equal(t, "https://google.com/", url, "[TestS3GetAsURLObjectSuccess] URL should \"https://google.com/\"")

	s3Client.AssertExpectations(t)
}

func TestS3GetAsURLObjectError(t *testing.T) {
	s3Client := new(mocks.StorageS3PresignObjectClient)

	s3Client.On("PresignGetObject", mock.Anything, mock.Anything).
		Return(nil, fmt.Errorf("test"))

	var s3ObjClient rstorager.CreateStorageS3PresignClientObjectClient = func(ctx context.Context, conf *aws.Config, optFns ...func(*s3.PresignOptions)) rstorager.StorageS3PresignObjectClient {
		return s3Client
	}

	s3 := rstorager.NewS3V2Storage(
		nil,
		nil,
		s3ObjClient,
		"tes",
	)

	url, err := s3.GetObjectAsURL(context.Background(), "")

	assert.Error(t, err, "[TestS3GetAsURLObjectError] Should Error")
	assert.Empty(t, url, "[TestS3GetAsURLObjectError] URL should empty")

	s3Client.AssertExpectations(t)
}
